#include "apriltag.h"

#ifdef USE_HALIDE
#include "Halide.h"

// #define HALIDE_GPU
// #define HALIDE_INLINE

#define TILE_SZ 4
#define DEBUG 1

// #define PRINT_DEBUG

#ifdef PRINT_DEBUG
#define IMG_WIDTH   16 //(3840/4)//398
#define IMG_HEIGHT  IMG_WIDTH //(2160/4)//224
#else
#define IMG_WIDTH   3840//398
#define IMG_HEIGHT  2160//224
#endif

using namespace Halide;

static double lapse;

image_u8_t *threshold_halide(apriltag_detector_t *td, image_u8_t *im);

#define STOPWATCH(X) Stopwatch stopwatch##__LINE__(X);

class Stopwatch
{
    double *dump_;
    std::chrono::system_clock::time_point start, finish;
public:
    Stopwatch(double * dump)
    {   
        dump_ = dump;
        start = std::chrono::high_resolution_clock::now();
    }
    ~Stopwatch()
    {
        auto finish = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = finish - start;
        *dump_ = elapsed.count();
    }
};

class HalideThresholder {
    Var x, y, yo, yi, xo, xi, tx, ty;
    RDom tile, tiles, grid33;
    Func clamped, thresh, local_min_max, local_blur, local_diff, local_thresh, binary_thresh, trinary_thresh;
    Buffer<uint8_t> input, thresh_buffer;
    int num_pixels, wb_diff;
    Target target;

    Func local_min, local_max, local_min_blur, local_max_blur;
public:
    HalideThresholder(uint8_t *buff, int w, int h);
    bool schedule_for_gpu();
    bool schedule_for_cpu();
    image_u8_t * run(apriltag_detector_t *td, image_u8_t *im);
    image_u8_t * run_debug(apriltag_detector_t *td, image_u8_t *im);
};

extern HalideThresholder *global_thresholder_p;

#endif /* USE_HALIDE */
