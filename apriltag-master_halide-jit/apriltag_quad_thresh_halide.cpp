#include "apriltag_quad_thresh_halide.h"

HalideThresholder::HalideThresholder(uint8_t *buff, int w, int h) 
    : tile(0, TILE_SZ, 0, TILE_SZ, "tile")
    , tiles(0, IMG_WIDTH / TILE_SZ, 0, IMG_HEIGHT / TILE_SZ, "tiles")
    , grid33(-1, 3, -1, 3, "grid33")
    , input(IMG_WIDTH, IMG_HEIGHT,/*buff, (std::vector<int>){w, h}, */"input")
    , x("x"), y("y"), xo("xo"), yo("yo"), xi("xi"), yi("yi"), tx("tx"), ty("ty")
    , clamped("clamped"), thresh("thresh")
    , local_min_max("local_min_max"), local_blur("local_blur")
    , local_diff("local_diff"), local_thresh("local_thresh")
    , binary_thresh("binary_thresh")
    , local_min("local_min"), local_max("local_max"), local_min_blur("local_min_blur"), local_max_blur("local_max_blur")
{
    num_pixels = input.width() * input.height();

    clamped(x, y) = input( clamp(x, 0, input.width()-1), clamp(y, 0, input.height()-1));

#ifdef HALIDE_INLINE
    local_min_max(tx, ty) = Tuple(
        minimum(clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y)),
        maximum(clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y)));
    local_blur(tx, ty) = Tuple(
        minimum(local_min_max(tx + grid33.x, ty + grid33.y)[0]),
        maximum(local_min_max(tx + grid33.x, ty + grid33.y)[1]));
    local_diff(tx, ty) = local_blur(tx, ty)[1] - local_blur(tx, ty)[0];
    local_thresh(tx, ty) = ( local_diff(tx, ty) / 2 ) + local_blur(tx, ty)[0];
#else
    local_min(tx, ty) = cast<uint8_t>(255);
    local_max(tx, ty) = cast<uint8_t>(0);
    local_min(tx, ty) = select(clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y) < local_min(tx, ty), clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y), local_min(tx, ty));
    local_max(tx, ty) = select(clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y) > local_max(tx, ty), clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y), local_max(tx, ty));

    local_min_blur(tx, ty) = cast<uint8_t>(255);
    local_max_blur(tx, ty) = cast<uint8_t>(0);
    local_min_blur(tx, ty) = select(local_min(tx + grid33.x, ty + grid33.y) < local_min_blur(tx, ty), local_min(tx + grid33.x, ty + grid33.y), local_min_blur(tx, ty));
    local_max_blur(tx, ty) = select(local_max(tx + grid33.x, ty + grid33.y) > local_max_blur(tx, ty), local_max(tx + grid33.x, ty + grid33.y), local_max_blur(tx, ty));

    local_diff(tx, ty) = local_max_blur(tx, ty) - local_min_blur(tx, ty);
    local_thresh(tx, ty) = ( local_diff(tx, ty) / 2 ) + local_min_blur(tx, ty);
#endif

    binary_thresh(x, y) = select(clamped(x, y) > local_thresh(x / TILE_SZ, y / TILE_SZ), max(clamped(x, y),255), 0);
    trinary_thresh(x, y) = select(local_diff(x / TILE_SZ, y / TILE_SZ) < (uint8_t)wb_diff, 127, binary_thresh(x, y));    
    thresh(x, y) = trinary_thresh(x, y);

    bool success =
#ifdef HALIDE_GPU
    schedule_for_gpu();
#else
    schedule_for_cpu();
#endif
    if(success)
        printf("Schedule created.\n");
    else printf("Failed to create schedule.\n");
    printf("\n");
}

bool HalideThresholder::schedule_for_gpu() 
{
    printf("Finding GPU host.\n");
    target = get_host_target();
    target.set_feature(Target::CUDA);
    if (!target.has_gpu_feature()) return false;
    printf("Target: %s\n", target.to_string().c_str());

    printf("Setting up schedule.\n");
    clamped.compute_root();

    printf("Tiling schedule.\n");
    thresh.gpu_tile(x, y, yo, xo, yi, xi, TILE_SZ, TILE_SZ);

    printf("Compiling JIT.\n");
    thresh.compile_jit(target);
    return true;
}

bool HalideThresholder::schedule_for_cpu() 
{
    printf("Setting up schedule.\n");
    thresh.tile(x, y, xo, yo, xi, yi, TILE_SZ, TILE_SZ).parallel(yo).parallel(xo);
    // local_diff.compute_at(thresh, xo);

    printf("Compiling stmt.\n");
    thresh.compile_to_lowered_stmt("threshold_halide.html", {}, Halide::HTML);        
    return true;
}

image_u8_t * HalideThresholder::run(apriltag_detector_t *td, image_u8_t *im)
{
    wb_diff = td->qtp.min_white_black_diff;
    int w = im->width, h = im->height, s = im->stride;
    image_u8_t *threshim = image_u8_create_alignment(w, h, s);
    input = Buffer<uint8_t>(im->buf, (std::vector<int>){im->width, im->height});
#ifdef HALIDE_GPU
        input.copy_to_device(target);
#endif
    thresh_buffer = thresh.realize(input.width(), input.height());
#ifdef HALIDE_GPU
        input.copy_to_host();
#endif
    memcpy(threshim->buf, (uint8_t*)thresh_buffer.get()->begin(), num_pixels);
    return threshim;
}

image_u8_t * HalideThresholder::run_debug(apriltag_detector_t *td, image_u8_t *im)
{
    int64_t start, finish;
    start = utime_now();

    if(td->debug) printf("Running thresholder.\n");
    int w = im->width, h = im->height, s = im->stride;
    assert(w < 32768);
    assert(h < 32768);
    wb_diff = td->qtp.min_white_black_diff;

    image_u8_t *threshim;
    { STOPWATCH(&lapse)
        threshim = image_u8_create_alignment(w, h, s);
    }
    if(DEBUG) std::cout << "Buffer allocation time: " << lapse*1000 << "ms" << std::endl;

    assert(threshim->stride == s);
    if(td->debug) printf("Asserts passed.\n");

    if(td->debug) printf("Transferring image data.\n");
    { STOPWATCH(&lapse)

       // input = Buffer<uint8_t>(im->buf, (std::vector<int>){im->width, im->height});
#ifdef HALIDE_GPU
       // input.copy_to_device(target);
#endif
        memcpy((uint8_t*)input.get()->begin(), im->buf, num_pixels);
    }
    if(DEBUG) std::cout << "Transfer time: " << lapse*1000 << "ms" << std::endl;

#ifdef PRINT_DEBUG
    for(int iy = 0; iy < input.height(); iy++)
    {
        for(int ix = 0; ix < input.width(); ix++)
        {
            printf("%3d ", input(ix, iy));
        }
        printf("\n");
    }
    printf("\n");

    // Realization min_max_output = local_min_max.realize(input.width() / TILE_SZ, input.height()/ TILE_SZ);
    // Buffer<uint8_t> mins = min_max_output[0];
    // Buffer<uint8_t> maxs = min_max_output[1];
    Buffer<uint8_t> diff = local_diff.realize(input.width() / TILE_SZ, input.height()/ TILE_SZ);
    Buffer<uint8_t> l_thresh = local_thresh.realize(input.width() / TILE_SZ, input.height()/ TILE_SZ);
    Buffer<uint8_t> l_min = local_min.realize(input.width() / TILE_SZ, input.height()/ TILE_SZ);
    Buffer<uint8_t> l_max = local_max.realize(input.width() / TILE_SZ, input.height()/ TILE_SZ);

    for(int iy = 0; iy < l_min.height(); iy++)
    {
        for(int ix = 0; ix < l_min.width(); ix++)
            printf("%3d|%3d  ", /*mins(ix, iy),  maxs(ix, iy),*/ l_min(ix, iy), l_max(ix, iy));//diff(ix, iy), l_thresh(ix, iy));
        printf("\n");
    }
    printf("\n");
#endif

    if(td->debug) printf("Realizing.\n");
    { STOPWATCH(&lapse)
        thresh_buffer = thresh.realize(input.width(), input.height());
    }
    if(DEBUG) std::cout << "Realization time: " << lapse*1000 << "ms <" << std::endl;

    if(td->debug) printf("Transferring thresholded data.\n");
    { STOPWATCH(&lapse)
#ifdef HALIDE_GPU
        thresh_buffer.copy_to_host();
#endif
        memcpy(threshim->buf, (uint8_t*)thresh_buffer.get()->begin(), num_pixels);
    }
    if(DEBUG) std::cout << "Transfer time: " << lapse*1000 << "ms" << std::endl;

    finish = utime_now();
    printf("Internal threshold time: %.3fms\n", (double)(finish-start) / 1000.);


#ifdef PRINT_DEBUG
    int p = 0;
    for(int iy = 0; iy < h; iy++)
    {
        for(int ix = 0; ix < w; ix++)
            printf("%3d ", threshim->buf[p++]);
        printf("\n");
    }

    printf("%d, %d, %d\n", w, h, num_pixels);
#endif

    return threshim;
}

HalideThresholder *global_thresholder_p = nullptr;

image_u8_t *threshold_halide(apriltag_detector_t *td, image_u8_t *im)
{
    return global_thresholder_p->run_debug(td, im);
}
