#include <math.h>
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "apriltag.h"
#include "common/image_u8x3.h"
#include "common/zarray.h"
#include "common/zhash.h"
#include "common/unionfind.h"
#include "common/timeprofile.h"
#include "common/zmaxheap.h"
#include "common/postscript_utils.h"
#include "common/math_util.h"

#define TILE_SIZE 16

typedef image_u8_t image_u8_cuda_t;

#ifdef DEBUG_CUDA
void printb(const char * s, uint8_t * buf, uint16_t w, uint16_t h)
{
	printf("%s (%d, %d):\n", s, w, h);
	for(uint16_t y = 0; y < h; y++)
	{
		printf("\t");
		for(uint16_t x = 0; x < w; x++)
		{
			int i = y * w + x;
			printf(" %3d", buf[i]);
		}
		printf("\n");
	}
	printf("\n");
}
#endif

image_u8_cuda_t* image_u8_create_and_align_cuda(image_u8_t* im)
{
	uint16_t stride = im->width;

	if ((stride % im->stride) != 0)
		stride += im->stride - (stride % im->stride);

	uint8_t* buf;
	size_t N = im->height * im->stride;
	cudaMalloc(&buf, N * sizeof(uint8_t));

	// const initializer
	image_u8_t tmp = { .width = im->width, .height = im->height, .stride = im->stride, .buf = buf };

	image_u8_cuda_t* im_cuda = (image_u8_cuda_t*)malloc(sizeof(image_u8_cuda_t));
	memcpy(im_cuda, &tmp, sizeof(image_u8_t));
	return im_cuda;
}

image_u8_t* transfer_image_u8_cuda_to_host(image_u8_cuda_t* im)
{
	cudaDeviceSynchronize();
	size_t N = im->height * im->stride;
	uint8_t* buf = (uint8_t*)calloc(N, sizeof(uint8_t));
	
	cudaError_t err = cudaMemcpy(buf, im->buf, N * sizeof(uint8_t), cudaMemcpyDeviceToHost);
	cudaFree(im->buf);
	im->buf = buf;
	return (image_u8_t*)im;
}

__global__ void min_max_cuda(uint8_t* im_buf, uint8_t* raw_min, uint8_t* raw_max, uint16_t w, uint16_t h, uint16_t s)
{
	uint16_t x = blockIdx.x * blockDim.x;
	uint16_t y = blockIdx.y * blockDim.y;
	uint16_t b = y * w + x;	
	uint8_t x_, y_, min = 255, max = 0, v;

	for(y_ = 0; y_ < TILE_SIZE; y_++)
	{
		for(x_ = 0; x_ < TILE_SIZE; x_++)
		{
			v = im_buf[(y * TILE_SIZE + y_) * s + (x * TILE_SIZE + x_)];
			if (v < min)
				min = v;
			if (v > max)
				max = v;
		}
	}
	
	raw_min[b] = min;
	raw_max[b] = max;
}

__global__ void blur_tiles_cuda(uint8_t* raw_min, uint8_t* raw_max, uint8_t* blur_min, uint8_t* blur_max, uint16_t w, uint16_t h)
{
	uint16_t x = blockIdx.x;
	uint16_t y = blockIdx.y;

	if (x < 1 || x >= w - 1 || y < 1 || y >= h - 1) return;
	
	int b = ( y - 1 ) * w + x - 1;
	uint8_t dx, dy, min = 255, max = 0;

	for(dy = 0; dy < 2; dy++, b += w-3)
	{
		for(dx = 0; dx < 2; dx++, b++)
		{
			if (raw_max[b] > max) max = raw_max[b];
			if (raw_min[b] < min) min = raw_min[b];
		}
	}

	b = y * w + x;
	blur_min[b] = min;
	blur_max[b] = max;
}

__global__ void threshold_tiles_cuda(uint8_t* blur_min, uint8_t* blur_max, uint8_t* im_buf_in, uint8_t* im_buf_out, uint16_t tw, uint16_t th, uint16_t s, uint8_t min_diff)
{
	uint16_t x = blockIdx.x * blockDim.x + threadIdx.x;
	uint16_t y = blockIdx.y * blockDim.y + threadIdx.y;

	uint16_t b = blockIdx.y * tw + blockIdx.x;
	uint16_t i = y * s + x;

	uint16_t diff = blur_max[b] - blur_min[b];

	if (diff < min_diff)
		im_buf_out[i] = 127;
	else // otherwise, actually threshold this tile.
	{
		// argument for biasing towards dark; specular highlights
		// can be substantially brighter than white tag parts
		if (im_buf_in[i] > blur_min[b] + (diff >> 1))
			im_buf_out[i] = 255;
		else
			im_buf_out[i] = 0;
	}
}

image_u8_t* threshold_cuda(apriltag_detector_t* td, image_u8_t* im)
{
	int w = im->width, h = im->height, s = im->stride;
	assert(w > 0);

	image_u8_cuda_t* threshim = image_u8_create_and_align_cuda(im);
	assert(threshim->stride == s);

	uint16_t N = w * h, BN = N * sizeof(uint8_t);

	uint8_t* im_cuda_buf;
	cudaMalloc((void**)&im_cuda_buf, BN);
	cudaMemcpy(im_cuda_buf, im->buf, BN, cudaMemcpyHostToDevice);	

	uint16_t tw = w / TILE_SIZE;
	uint16_t th = h / TILE_SIZE;
	uint16_t tN = tw * th;
	uint16_t tBN = sizeof(uint8_t)*tN;

	uint8_t* raw_min;
	uint8_t* raw_max;
	uint8_t* blur_min;
	uint8_t* blur_max;
	cudaMalloc((void**)&raw_min, tBN);
	cudaMalloc((void**)&raw_max, tBN);
	cudaMalloc((void**)&blur_min, tBN);
	cudaMalloc((void**)&blur_max, tBN);
	cudaMemset(raw_min, 255, tBN);
	cudaMemset(raw_max, 0, tBN);
	cudaMemset(blur_min, 255, tBN);
	cudaMemset(blur_max, 0, tBN);

	dim3 tGrid(tw, th);
	dim3 tBlock(TILE_SIZE, TILE_SIZE);
	dim3 uBlock(1, 1);
	
	// first, collect min/max statistics for each tile
	//threshold_tiles_cuda<<<tGrid, tBlock>>>(im_cuda_buf, raw_min, raw_max, w, h, s, tw);
	min_max_cuda<<<tGrid, uBlock>>>(im_cuda_buf, raw_min, raw_max, tw, th, s);

	// second, apply 3x3 max/min convolution to "blur" these values
	// over larger areas. This reduces artifacts due to abrupt changes
	// in the threshold value.
	blur_tiles_cuda<<<tGrid, uBlock>>>(raw_min, raw_max, blur_min, blur_max, tw, th);
	
	// third, apply threshold with low contrast differentiation
	threshold_tiles_cuda<<<tGrid, tBlock>>>(blur_min, blur_max, im_cuda_buf, threshim->buf, tw, th, s, td->qtp.min_white_black_diff);

	/// Skip incomplete tiles (relies on absolute tile multiple check above)

	cudaDeviceSynchronize();

#ifdef DEBUG_CUDA
	cudaError_t err;
	printb("CPU Original Buffer", im->buf, w, h);
	memset(im->buf, 0, BN);
	err = cudaMemcpy(im->buf, im_cuda_buf, BN, cudaMemcpyDeviceToHost);	
	printf("Error: %d\n", (int)err);
	printb("GPU Original Buffer", im->buf, w, h);
	
	uint8_t* min_buf = (uint8_t*)malloc(tBN);
	cudaMemcpy(min_buf, blur_min, tBN, cudaMemcpyDeviceToHost);
	printb("Min Buffer", min_buf, tw, th);
	free(min_buf);

	uint8_t* max_buf = (uint8_t*)malloc(tBN);
	cudaMemcpy(max_buf, blur_max, tBN, cudaMemcpyDeviceToHost);
	printb("Max Buffer", max_buf, tw, th);
	free(max_buf);


	err = cudaMemcpy(im->buf, threshim->buf, BN, cudaMemcpyDeviceToHost);	
	printf("Error: %d\n", (int)err);
	printb("GPU End Buffer", threshim->buf, w, h);
#endif

	threshim = transfer_image_u8_cuda_to_host(threshim);

	cudaFree(im_cuda_buf);

	cudaFree(raw_min);
	cudaFree(raw_max);

	cudaFree(blur_min);
	cudaFree(blur_max);

	timeprofile_stamp(td->tp, "threshold");

	return threshim;
}