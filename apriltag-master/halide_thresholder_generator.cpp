#include <Halide.h>

#define TILE_SZ 4

// #define HALIDE_INLINE

#ifdef PRINT_DEBUG
    #define IMG_WIDTH   16 //(3840/4)//398
    #define IMG_HEIGHT  IMG_WIDTH //(2160/4)//224
#else
    #define IMG_WIDTH   3840//398
    #define IMG_HEIGHT  2160//224
#endif

namespace 
{
    Halide::RDom tile(0, TILE_SZ, 0, TILE_SZ, "tile");
    Halide::RDom tiles(0, IMG_WIDTH / TILE_SZ, 0, IMG_HEIGHT / TILE_SZ, "tiles");
    Halide::RDom grid33(-1, 3, -1, 3, "grid33");
    Halide::Var x, y, yo, yi, xo, xi, tx, ty;

    Halide::Func clamped, thresh, local_min_max, local_blur, local_diff, local_thresh, binary_thresh, trinary_thresh, local_min, local_max, local_min_blur, local_max_blur;

    class HalideThresholder : public Halide::Generator<HalideThresholder> {

    public:
        Input<Buffer<uint8_t>> in_{"in", 2};
        Output<Buffer<uint8_t>> out_{"out", 2};

        void generate()
        {
            clamped(x, y) = in_( clamp(x, 0, in_.width()-1), clamp(y, 0, in_.height()-1));

#ifdef HALIDE_INLINE
            local_min_max(tx, ty) = Tuple(
                minimum(clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y)),
                maximum(clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y)));
            local_blur(tx, ty) = Tuple(
                minimum(local_min_max(tx + grid33.x, ty + grid33.y)[0]),
                maximum(local_min_max(tx + grid33.x, ty + grid33.y)[1]));
            local_diff(tx, ty) = local_blur(tx, ty)[1] - local_blur(tx, ty)[0];
            local_thresh(tx, ty) = ( local_diff(tx, ty) / 2 ) + local_blur(tx, ty)[0];
#else
            local_min(tx, ty) = cast<uint8_t>(255);
            local_max(tx, ty) = cast<uint8_t>(0);
            local_min(tx, ty) = select(clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y) < local_min(tx, ty), clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y), local_min(tx, ty));
            local_max(tx, ty) = select(clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y) > local_max(tx, ty), clamped(tx * TILE_SZ + tile.x, ty * TILE_SZ + tile.y), local_max(tx, ty));

            local_min_blur(tx, ty) = cast<uint8_t>(255);
            local_max_blur(tx, ty) = cast<uint8_t>(0);
            local_min_blur(tx, ty) = select(local_min(tx + grid33.x, ty + grid33.y) < local_min_blur(tx, ty), local_min(tx + grid33.x, ty + grid33.y), local_min_blur(tx, ty));
            local_max_blur(tx, ty) = select(local_max(tx + grid33.x, ty + grid33.y) > local_max_blur(tx, ty), local_max(tx + grid33.x, ty + grid33.y), local_max_blur(tx, ty));

            local_diff(tx, ty) = local_max_blur(tx, ty) - local_min_blur(tx, ty);
            local_thresh(tx, ty) = ( local_diff(tx, ty) / 2 ) + local_min_blur(tx, ty);
#endif

            binary_thresh(x, y) = select(clamped(x, y) > local_thresh(x / TILE_SZ, y / TILE_SZ), max(clamped(x, y), 255), 0);
            trinary_thresh(x, y) = select(local_diff(x / TILE_SZ, y / TILE_SZ) < 5, 127, binary_thresh(x, y));    
            out_(x, y) = trinary_thresh(x, y);

            local_diff.compute_at(out_, xo);

            out_.gpu_tile(x, y, xo, yo, xi, yi, TILE_SZ, TILE_SZ);
        }
    };
} /* anonymous namespace */

HALIDE_REGISTER_GENERATOR(HalideThresholder, halide_thresholder)
// #endif /* USE_HALIDE */