#include "apriltag.h"

#include "HalideBuffer.h"
#include "halide_benchmark.h"
#include "halide_thresholder.h"

#define TILE_SZ 4

#ifdef PRINT_DEBUG
    #define IMG_WIDTH   16 //(3840/4)//398
    #define IMG_HEIGHT  IMG_WIDTH //(2160/4)//224
#else
    #define IMG_WIDTH   3840//398
    #define IMG_HEIGHT  2160//224
#endif

using namespace Halide::Runtime;
using namespace Halide::Tools;

image_u8_t *threshold_halide(apriltag_detector_t *td, image_u8_t *im)
{
    int w = im->width, h = im->height, s = im->stride;
    image_u8_t *threshim = image_u8_create_alignment(w, h, s);
    Buffer<uint8_t> input(im->buf, (std::vector<int>){im->width, im->height}), thresh_buffer(IMG_WIDTH, IMG_HEIGHT);
    halide_thresholder(input, thresh_buffer);
    memcpy(threshim->buf, (uint8_t*)thresh_buffer.begin(), w * h);
    return threshim;
}

// #endif /* USE_HALIDE */
