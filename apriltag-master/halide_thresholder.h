#ifndef HALIDE____home___matthew___Desktop___lib___apriltag___master_______genfiles___halide_thresholder___halide_thresholder_h
#define HALIDE____home___matthew___Desktop___lib___apriltag___master_______genfiles___halide_thresholder___halide_thresholder_h
#include <stdint.h>

// Forward declarations of the types used in the interface
// to the Halide pipeline.
//
// For the definitions of these structs, include HalideRuntime.h

// Halide's representation of a multi-dimensional array.
// Halide::Runtime::Buffer is a more user-friendly wrapper
// around this. Its declaration is in HalideBuffer.h
struct halide_buffer_t;

// Metadata describing the arguments to the generated function.
// Used to construct calls to the _argv version of the function.
struct halide_filter_metadata_t;

#ifndef HALIDE_FUNCTION_ATTRS
#define HALIDE_FUNCTION_ATTRS
#endif



#ifdef __cplusplus
extern "C" {
#endif

int halide_thresholder(struct halide_buffer_t *_in_buffer, struct halide_buffer_t *_out_buffer) HALIDE_FUNCTION_ATTRS;
int halide_thresholder_argv(void **args) HALIDE_FUNCTION_ATTRS;
const struct halide_filter_metadata_t *halide_thresholder_metadata() HALIDE_FUNCTION_ATTRS;

#ifdef __cplusplus
}  // extern "C"
#endif

#endif
